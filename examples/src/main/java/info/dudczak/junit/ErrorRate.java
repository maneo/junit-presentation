package info.dudczak.junit;

public class ErrorRate {

    double errorRate;

    public ErrorRate(long noOfItems, long noOfErrors) {
        errorRate = calculateErrorRate(noOfItems, noOfErrors);
    }

    private final double calculateErrorRate(long noOfItems, long noOfErrors) {
        if (noOfItems == 0) {
            return 0;
        }
        return ((double) noOfErrors) / noOfItems;
    }

    public String getErrorRateAsString() {
        return String.format("%2.02f", errorRate);
    }

    public double getErrorRateAsDouble() {
        return errorRate;
    }
}
