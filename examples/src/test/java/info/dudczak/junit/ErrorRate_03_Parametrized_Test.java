package info.dudczak.junit;

import java.util.Arrays;
import java.util.Collection;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ErrorRate_03_Parametrized_Test {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            new Object[]{100, 1, "0,01"},
            new Object[]{0, 0, "0,00"},});
    }

    long totalNumberOfItems;
    long totalNumberOfRejected;
    String finalErrorRate;

    public ErrorRate_03_Parametrized_Test(long totalNumberOfItems,
            long totalNumberOfRejected,
            String finalErrorRate) {

        this.totalNumberOfItems = totalNumberOfItems;
        this.totalNumberOfRejected = totalNumberOfRejected;
        this.finalErrorRate = finalErrorRate;
    }

    @Test
    public void shouldCalculateErrorRate() {
        //given
        ErrorRate errorRate = new ErrorRate(totalNumberOfItems,
                totalNumberOfRejected);
        //when
        String result = errorRate.getErrorRateAsString();

        //then
        assertThat(result, is(finalErrorRate));
    }

}
