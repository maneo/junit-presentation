package info.dudczak.junit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import com.carrotsearch.randomizedtesting.RandomizedContext;
import com.carrotsearch.randomizedtesting.RandomizedTest;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class ErrorRate_05_Theory_Test {

    @DataPoints public static int[] someNumbers = new int[] {-1, 10, 120, 340, -10, -100};

    @DataPoints public static int[] otherNumbers = new int[] {0, 5, 100, 1200, -10};

    @Theory
    public void resultShouldBeSmallerThan1(int noOfItems, int noOfErrors) {
        assumeTrue(noOfErrors <= noOfItems);
        assumeTrue(noOfErrors > 0);
        assumeTrue(noOfItems > 0);
        
        System.out.println("noOfItems: "+noOfItems+" noOfErrors: "+noOfErrors);
        
        ErrorRate errorRate = new ErrorRate(noOfItems, noOfErrors);
        double result = errorRate.getErrorRateAsDouble();
        
        boolean isSmallerThan1 =  Double.compare(result, 1) <= 0;

        assertTrue("result should be smaller than 1",isSmallerThan1);
    }
    
    @Theory
    public void resultShouldBeGreaterThan0(int noOfItems, int noOfErrors) {
        assumeTrue(noOfErrors <= noOfItems);
        assumeTrue(noOfErrors > 0);
        assumeTrue(noOfItems > 0);
        
        System.out.println("noOfItems: "+noOfItems+" noOfErrors: "+noOfErrors);
        
        ErrorRate errorRate = new ErrorRate(noOfItems, noOfErrors);
        double result = errorRate.getErrorRateAsDouble();
        
        boolean isGreaterThan0 =  Double.compare(result, 0) > 0;

        assertTrue("result should be greater than 0", isGreaterThan0);
    }
    
}
