package info.dudczak.junit.order;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MethodOrderTest {

    @Test
    public void registration_step1_shouldSendActivationLink() {
        System.out.println("verify that link was send");
    }

    @Test
    public void registration_step2_shouldCreateNewUserAccount() {
        System.out.println("verify user account creation");
    }

    @Test
    public void registration_step3_shouldBeAbleToLogAsANewUser() {
        System.out.println("verify user login");
    }

}
