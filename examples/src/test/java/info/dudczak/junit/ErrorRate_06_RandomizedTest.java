package info.dudczak.junit;

import com.carrotsearch.randomizedtesting.RandomizedRunner;
import com.carrotsearch.randomizedtesting.RandomizedTest;
import com.carrotsearch.randomizedtesting.annotations.Nightly;
import com.carrotsearch.randomizedtesting.annotations.Repeat;
import com.carrotsearch.randomizedtesting.annotations.ThreadLeakAction;
import com.carrotsearch.randomizedtesting.annotations.ThreadLeakLingering;
import com.carrotsearch.randomizedtesting.annotations.ThreadLeakScope;
import com.carrotsearch.randomizedtesting.annotations.ThreadLeakZombies;
import org.junit.Test;
import org.junit.runner.RunWith;

@ThreadLeakScope(ThreadLeakScope.Scope.TEST)
@ThreadLeakZombies(ThreadLeakZombies.Consequence.IGNORE_REMAINING_TESTS)
public class ErrorRate_06_RandomizedTest extends RandomizedTest {

    @Test
    @ThreadLeakLingering(linger = 2000)
    @ThreadLeakAction(ThreadLeakAction.Action.WARN)
    @Repeat(iterations = 10)
    public void errorRateShouldBeLowerThan1() {
        //given
        int numberOfItems  = randomIntBetween(0, Integer.MAX_VALUE);
        int numberOfErrors = randomIntBetween(0, numberOfItems);
        ErrorRate errorRate = new ErrorRate(numberOfItems, numberOfErrors);
        //when
        double result = errorRate.getErrorRateAsDouble();
        //then
        assertTrue("result > 1.0", result < 1.0);
    }

    @Test
    @Nightly
    public void errorRateIntensiveTest() {

    }

}
