package info.dudczak.junit.rules;

import java.io.File;
import java.io.IOException;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.TestName;
import org.junit.rules.Timeout;

public class JunitRulesShowcaseTest {

    /**
     * Normal rule does not work as static field (cannot be used in 
     * @BeforeClass). Use ClassRule instead. It also applies to test suites.
     */
    @ClassRule
    public static TemporaryFolder tempFolder = new TemporaryFolder();

    @Rule
    public TestName name = new TestName();

    @Rule
    public Timeout globalTimeout = new Timeout(100);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static File workingDir;

    @BeforeClass
    public static void setupWorkingDir() throws IOException {
        workingDir = tempFolder.newFolder();
    }

    @Test
    public void shouldOutputNamesOfTests() {
        System.out.println("Running test " + name.getMethodName() + " in " + workingDir);
    }

    @Test @Ignore
    public void testInfiniteLoop1() {
        int counter = 0;
        for (;;) {
            System.out.println("counter: " + counter++);
        }
    }

    @Test
    public void verifiesTypeAndMessage() {
 
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Runtime exception occurred");
         
        throw new RuntimeException("Runtime exception occurred");
    }

 }
