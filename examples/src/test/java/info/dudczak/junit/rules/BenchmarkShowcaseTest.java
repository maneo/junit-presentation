package info.dudczak.junit.rules;

import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;
import com.carrotsearch.junitbenchmarks.annotation.AxisRange;
import com.carrotsearch.junitbenchmarks.annotation.BenchmarkMethodChart;
import com.carrotsearch.randomizedtesting.RandomizedTest;
import com.carrotsearch.randomizedtesting.annotations.ThreadLeakScope;
import info.dudczak.junit.ErrorRate;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

@AxisRange(min = 0, max = 1)
@BenchmarkMethodChart(filePrefix = "benchmark-lists")
@ThreadLeakScope(ThreadLeakScope.Scope.NONE)
public class BenchmarkShowcaseTest extends RandomizedTest{

    @Rule
    public TestRule benchmarkRun = new BenchmarkRule();

    @Test
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 3)
    public void serializeAsDouble() {
        int numberOfItems  = randomIntBetween(0, Integer.MAX_VALUE);
        int numberOfErrors = randomIntBetween(0, numberOfItems);
        ErrorRate errorRate = new ErrorRate(numberOfItems, numberOfErrors);

        //benchmark
        errorRate.getErrorRateAsDouble();

        //errorRate is fast and furious slow it down a bit for nice charts
        delay();
    }

    @Test
    @BenchmarkOptions(callgc = false, benchmarkRounds = 20, warmupRounds = 3)
    public void serializeAsString() throws InterruptedException {
        int numberOfItems  = randomIntBetween(0, Integer.MAX_VALUE);
        int numberOfErrors = randomIntBetween(0, numberOfItems);
        ErrorRate errorRate = new ErrorRate(numberOfItems, numberOfErrors);

        //benchmark
        errorRate.getErrorRateAsString();

        //errorRate is fast and furious slow it down a bit for nice charts
        delay();
    }

    private void delay() {
        try {
            Thread.sleep(randomIntBetween(0, 100));
        } catch (InterruptedException e) {
            throw new RuntimeException("No Thread.sleep today");
        }
    }

}
