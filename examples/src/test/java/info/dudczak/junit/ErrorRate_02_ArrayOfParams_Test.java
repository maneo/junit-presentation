package info.dudczak.junit;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class ErrorRate_02_ArrayOfParams_Test {

     Object[][] testParameters = new Object[][]{
        new Object[]{100, 1, "0,01"},
        new Object[]{0, 0, "0,00"},};

    @Test
    public void shouldCalculateErrorRate1() {
         for (Object[] testParameter : testParameters) {
             
             int noOfItems = (Integer) testParameter[0];
             int noOfErrors = (Integer) testParameter[1];
             
             ErrorRate errorRate = new ErrorRate(noOfItems,noOfErrors);
             String result = errorRate.getErrorRateAsString();
             
             String expectedResult = (String) testParameter[2];
             
             assertThat(result, is(expectedResult));
         }
    }
    
}
