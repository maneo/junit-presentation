package info.dudczak.junit.suites;


import info.dudczak.junit.ErrorRate_01_SimpleTest;
import org.junit.ClassRule;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.rules.ExternalResource;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Categories.class)
@IncludeCategory(SuiteWithCategories.HeavyWeight.class)
@SuiteClasses( { ErrorRate_01_SimpleTest.class, HeavyIntegrationTest.class} )
public class SuiteWithCategories {

    //category marker interface
    public interface HeavyWeight {}

    @ClassRule
    public static ExternalResource resource= new ExternalResource() {
        @Override
        protected void before() throws Throwable {
            System.out.println("Starting the heavyweightServer");
        };

        @Override
        protected void after() {
            System.out.println("Stopping the heavyweightServer");
        };
    };

}
