package info.dudczak.junit.suites;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import static org.junit.Assume.assumeTrue;

public class HeavyIntegrationTest {

    @Test
    @Category(SuiteWithCategories.HeavyWeight.class)
    public void shouldCalculateErrorRate() {
        assumeTrue(isHeavyWeightServerRunning());
        //heave stuff with heavyWeight server here
    }

    private boolean isHeavyWeightServerRunning() {
        //check if server is running
        //should be true only when running from suite
        return true;
    }
}
