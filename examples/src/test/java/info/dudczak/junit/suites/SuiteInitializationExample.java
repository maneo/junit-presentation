package info.dudczak.junit.suites;

import info.dudczak.junit.ErrorRate_01_SimpleTest;
import info.dudczak.junit.ErrorRate_03_Parametrized_Test;
import info.dudczak.junit.rules.BenchmarkShowcaseTest;
import info.dudczak.junit.rules.JunitRulesShowcaseTest;
import org.junit.ClassRule;
import org.junit.rules.ExternalResource;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@SuiteClasses({ ErrorRate_01_SimpleTest.class, ErrorRate_03_Parametrized_Test.class })
public class SuiteInitializationExample {

    @ClassRule
    public static ExternalResource resource= new ExternalResource() {
        @Override
        protected void before() throws Throwable {
            System.out.println("Starting the heavyweightServer");
        };

        @Override
        protected void after() {
            System.out.println("Stopping the heavyweightServer");
        };
    };

}
