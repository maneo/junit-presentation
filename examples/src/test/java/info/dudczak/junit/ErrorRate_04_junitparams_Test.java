package info.dudczak.junit;


import junitparams.JUnitParamsRunner;
import static junitparams.JUnitParamsRunner.$;
import junitparams.Parameters;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class ErrorRate_04_junitparams_Test {

    @Test
    @Parameters(method = "errorRateParameters")
    public void shouldCalculateErrorRate(int totalNumberOfItems,
                                         int totalNumberOfRejected,
                                         String result) {
        //given
        ErrorRate errorRate = new ErrorRate(totalNumberOfItems,
                totalNumberOfRejected);
        //when
        String errorRateAsString = errorRate.getErrorRateAsString();

        //then
        assertThat(errorRateAsString, is(result));
    }

    public Object errorRateParameters() {
        return $(
            $(100, 10, "0,10"),
            $(100, 1, "0,01"),
            $(0, 0, "0,00")
        );        
    }

}
