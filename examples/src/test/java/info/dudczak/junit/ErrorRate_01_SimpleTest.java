package info.dudczak.junit;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ErrorRate_01_SimpleTest {

    @Test
    public void shouldCalculateErrorRate() {
        //given
        ErrorRate errorRate = new ErrorRate(100, 10);

        //when
        String result = errorRate.getErrorRateAsString();

        //then
        assertTrue("0,10".equals(result));
    }

}
