Slides (slides/index.html) and source code examples from "JUnit: beyond the basics" presentation.

Presentation was delivered at:

11/09/2013 - internal PSNC DL Team presentation (inital 20min version)

07/01/2014 -  (30 min) [Poznań JUG meeting](http://www.jug.poznan.pl/2014/01/spotkanie-poznan-jug-jakub-nowak-clojure-5-things-you-have-to-know-adam-dudczak-junit-parametry-teorie-reguly-i-inni/)

27/02/2014 -  (30 min [Tricity JUG meeting](http://trojmiasto.jug.pl/2014/02/10/52-spotkanie-z-dwoma-prezentacjami-i-wejsciowka-na-geecona/)

12/04/2014 - (50 min) [Devcrowd 2014](http://2014.devcrowd.pl/prelegenci/#Adam+Dudczak)

19/05/2014 - (40 min) [Athmopshere 2014](http://atmosphere-conference.com)


